---
title: 【艺术欣赏】来自David-Ambarzumjan的系列油画Brushstrokes-in-time
typora-root-url: 【艺术欣赏】来自David-Ambarzumjan的系列油画Brushstrokes-in-time
cover: /img/blog_img/8.png
tags:
  - David Ambarzumjan
  - Brushstrokes in time
  - 油画
  - 时光笔触
categories:
  - 艺术欣赏
  - 油画艺术
abbrlink: efbf378f
date: 2023-07-06 02:28:02
---











​		本人只是一个普通的理工男，对于艺术，尤其是油画艺术并没有什么研究，只是从直观感受上判断一幅画好不好。在2023年7月5日的晚上，躺在床上刷B站的我突然刷到了一组视频，介绍了David Ambarzumjan绘制的《Brushstrokes in time》系列油画。当时就被震撼到了，这是继梵高的《星空》后，唯一感觉直冲我灵魂的画作，语言能力有限，没法很好的描述画作的意境，那也就不再献丑，就把图放上来让大家欣赏一下吧。



<table rules="none" align="center">
    <tr>
		<td colspan=2>
			<center>
				<img src="Recover.png"/>
				<br/>
				<font color="AAAAAA">Recover</font>
			</center>
		</td>
	</tr>
	<tr>
		<td>
			<center>
				<img src="Breach.png"/>
				<br/>
				<font color="AAAAAA">Breach</font>
			</center>
		</td>
		<td>
			<center>
				<img src="Breathe.png"/>
				<br/>
				<font color="AAAAAA">Breathe</font>
			</center>
		</td>
	</tr>
    <tr>
		<td>
			<center>
				<img src="Hazy Notion.png"/>
				<br/>
				<font color="AAAAAA">Hazy Notion</font>
			</center>
		</td>
		<td>
			<center>
				<img src="Running Out Of Time.png"/>
				<br/>
				<font color="AAAAAA">Running Out Of Time</font>
			</center>
		</td>
	</tr>
    <tr>
		<td>
			<center>
				<img src="Sharks in Montmartre.png"/>
				<br/>
				<font color="AAAAAA">Sharks in Montmartre</font>
			</center>
		</td>
		<td>
			<center>
				<img src="Starling.png"/>
				<br/>
				<font color="AAAAAA">Starling</font>
			</center>
		</td>
	</tr>
    <tr>
		<td>
			<center>
				<img src="Stray.png"/>
				<br/>
				<font color="AAAAAA">Stray</font>
			</center>
		</td>
		<td>
			<center>
				<img src="Inversion.png"/>
				<br/>
				<font color="AAAAAA">Inversion</font>
			</center>
		</td>
	</tr>
    <tr>
		<td>
			<center>
				<img src="Watershed.png"/>
				<br/>
				<font color="AAAAAA">Watershed</font>
			</center>
		</td>
		<td>
			<center>
				<img src="Zebra Crossing.png"/>
				<br/>
				<font color="AAAAAA">Zebra Crossing</font>
			</center>
		</td>
	</tr>
     <tr>
		<td>
			<center>
				<img src="This Was Water.png"/>
				<br/>
				<font color="AAAAAA">This Was Water</font>
			</center>
		</td>
		<td>
			<center>
				<img src="Water Over the Bridge.png"/>
				<br/>
				<font color="AAAAAA">Water Over the Bridge</font>
			</center>
		</td>
	</tr>
    <tr>
		<td>
			<center>
				<img src="Transience.png"/>
				<br/>
				<font color="AAAAAA">Transience</font>
			</center>
		</td>
		<td>
			<center>
				<img src="Human Nature.png"/>
				<br/>
				<font color="AAAAAA">Human Nature</font>
			</center>
		</td>
	</tr>
    <tr>
		<td>
			<center>
				<img src="Impact.png"/>
				<br/>
				<font color="AAAAAA">Impact</font>
			</center>
		</td>
		<td>
			<center>
				<img src="Gezeitenwelle.png"/>
				<br/>
				<font color="AAAAAA">Gezeitenwelle</font>
			</center>
		</td>
	</tr>
    <tr>
		<td>
			<center>
				<img src="Deserted.png"/>
				<br/>
				<font color="AAAAAA">Deserted</font>
			</center>
		</td>
		<td>
			<center>
				<img src="Zebra Crossing.png"/>
				<br/>
				<font color="AAAAAA">Zebra Crossing</font>
			</center>
		</td>
	</tr>
</table>



























